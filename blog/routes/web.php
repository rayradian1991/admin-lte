<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route ::get('/test', function(){

    return "OK";
});


Route ::get('/halo', function(){

    return "Halo";
});


Route ::get('/halo/{nama}',function($nama){
    return "halo $nama";
});

Route::get('/Tugas1', function(){

    return view('tugas1');

});


Route ::get('/form', 'RegisterController@form');
Route ::get('/anggota', 'RegisterController@anggota');


Route::get('/master', function(){

    return view('/adminlte/master');

});


Route::get('/items', function(){

    return view('items.index');
});

